import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemManagerComponent } from './item-manager/item-manager.component';
import { ListManagerComponent } from './list-manager/list-manager.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LunchSpotService } from './services/lunch-spot.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    ListManagerComponent,
    ItemManagerComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
      LunchSpotService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
