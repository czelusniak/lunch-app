import { Component, Input, OnInit } from '@angular/core';
import { LunchSpot } from '../models/lunch-spot';

@Component({
  selector: 'app-item-manager',
  templateUrl: './item-manager.component.html',
  styleUrls: ['./item-manager.component.css']
})
export class ItemManagerComponent implements OnInit {

    @Input() item: LunchSpot;

    constructor() { }

    ngOnInit() {
    }
}
