import { Component, OnInit } from '@angular/core';
import { LunchSpot, StatusType } from '../models/lunch-spot';
import { LunchSpotService } from '../services/lunch-spot.service';

@Component({
    selector: 'app-waiting-card',
    templateUrl: './awaiting.component.html',
    styleUrls: ['./awaiting.component.css']
})
export class AwaitingComponent implements OnInit {

    public lunchSpots: LunchSpot[];

    constructor(private lunchSpotService: LunchSpotService) { }

    ngOnInit() {
        this.lunchSpotService.getLunchSpots(StatusType.UNDELIVERED).subscribe({
            next: (lunchSpots) => this.lunchSpots = lunchSpots,
            error: () => alert('Nie udało się pobrać rekordów')
        });
    }

}
