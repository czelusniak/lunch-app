import { Component, OnInit } from '@angular/core';
import { LunchSpot, StatusType } from '../models/lunch-spot';
import { LunchSpotService } from '../services/lunch-spot.service';

@Component({
    selector: 'app-delivered-card',
    templateUrl: './delivered.component.html',
    styleUrls: ['./delivered.component.css']
})
export class DeliveredComponent implements OnInit {

    public lunchSpots: LunchSpot[];

    constructor(private lunchSpotService: LunchSpotService) { }

    ngOnInit() {
        this.lunchSpotService.getLunchSpots(StatusType.DELIVERED).subscribe({
            next: (lunchSpots) => this.lunchSpots = lunchSpots,
            error: () => alert('Nie udało się pobrać rekordów')
        });
    }

}
