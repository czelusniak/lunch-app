import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AwaitingComponent } from './awaiting/awaiting.component';
import { DeliveredComponent } from './delivered/delivered.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
    { path: 'wait', component: AwaitingComponent },
    { path: 'close', component: DeliveredComponent },
    { path: '', redirectTo: 'wait', pathMatch: 'full'},
    { path: '**', component: PageNotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [AwaitingComponent, DeliveredComponent];
