import { Component, Input, OnInit } from '@angular/core';
import { LunchSpot } from '../models/lunch-spot';

@Component({
  selector: 'app-list-manager',
  templateUrl: './list-manager.component.html',
  styleUrls: ['./list-manager.component.css']
})
export class ListManagerComponent implements OnInit {

    @Input() lunchSpots: LunchSpot[];

    constructor() { }

    ngOnInit() {
    }
}
