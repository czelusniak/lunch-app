import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, reduce } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LunchSpot, LunchSpotAttrs, StatusType } from '../models/lunch-spot';

@Injectable({
    providedIn: 'root'
})
export class LunchSpotService {

    constructor(private http: HttpClient) { }

    getLunchSpots(status: StatusType): Observable<LunchSpot[]> {
        return this.http.get<LunchSpotAttrs[]>('/api/lunchSpots').pipe(
            map(data => data.filter(lunchSpotAttrs => lunchSpotAttrs.status === status)),
            map(data => data.map(lunchSpotAttrs => new LunchSpot(lunchSpotAttrs)))
        );
    }
}
