import { TestBed } from '@angular/core/testing';

import { LunchSpotService } from './lunch-spot.service';

describe('LunchSpotService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LunchSpotService = TestBed.get(LunchSpotService);
    expect(service).toBeTruthy();
  });
});
