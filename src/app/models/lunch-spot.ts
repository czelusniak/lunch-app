export enum StatusType {
  UNDELIVERED = 'UNDELIVERED',
  DELIVERED = 'DELIVERED'
}

export interface LunchSpotAttrs {
  lunchSpotId: number;
  name: string;
  status: StatusType;
  address: string;
  company: string;
  deliveryTime: string;
}

export class LunchSpot {
  lunchSpotId: number;
  name: string;
  status: StatusType;
  address: string;
  company: string;
  deliveryTime: string;

  constructor(attrs: Partial<LunchSpotAttrs> = {}) {
    this.lunchSpotId = attrs.lunchSpotId;
    this.name = attrs.name;
    this.status = attrs.status;
    this.address = attrs.address;
    this.company = attrs.company;
    this.deliveryTime = attrs.deliveryTime;
  }
}
